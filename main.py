# Real estate app
from models.User import User
from models.Property import Property
from models.Contract import Contract


def welcome():
    print("Welcome :)")
    while True:
        print("What can i do for you?")
        print("1- Add new property")
        print("2- See all properties")
        print("3- Search in properties")
        print("4- Make a contract")
        print("0- For exit")
        user_selection = int(input(">"))

        if user_selection == 1:
            user_id = User.options() # make and save new user and returns user id
            contract = Contract.contract_options() # make and save contract returns contract object
            Property.add_new_property(user_id, contract.ctype, contract.id) # make and save new property return none

        elif user_selection == 2:
            Property.show_all_properties()

        elif user_selection == 3:
            pass
        elif user_selection == 0:
            print("Bye!")
            return
        else:
            print("Ooops! Invalid option.")
            return welcome()


if __name__ == "__main__":
    welcome()