from .storage import property_objects
from .storage import user_objects


class Property:
    id = 0

    def __init__(self, address, area, floor, construction_year, property_type,
                 warehouse, parking, elevator, seller_id, contract_type,
                 contract_no):
        self.id = Property.id
        self.address = address
        self.area = area
        self.floor = floor
        self.construction_year = construction_year
        self.property_type = property_type
        self.warehouse = warehouse
        self.parking = parking
        self.elevator = elevator
        self.seller_id = seller_id
        self.contract_type = contract_type
        self.contract_no = contract_no

        Property.id += 1

    @classmethod
    def add_new_property(cls, seller_id, contract_type, contract_no):
        address = input('Address: ')
        area = int(input('Area: '))
        floor = int(input('Floor: '))
        construction_year = int(input('Construction Year: '))
        property_type = input("Commercial Or Residential?(c/r)")
        warehouse = int(input("Warehouse: "))
        parking = int(input("Parking: "))
        elevator = input("Elevator?(y/n)")

        new_property = cls(address, area, floor, construction_year, property_type,
                           warehouse, parking, elevator, seller_id, contract_type,
                           contract_no)
        new_property.save()

    @staticmethod
    def show_all_properties():
        for id, property_obj in property_objects.items():
            print(f"{id}- Address: {property_obj.address} ", end='')
            print(f"- Seller: {user_objects[property_obj.seller_id]} ", end='')
            print(f"- Phone: {user_objects[property_obj.seller_id].phone}")

    def save(self):
        property_objects[self.id] = self

    def __str__(self):
        return f"Address: {self.address}"
