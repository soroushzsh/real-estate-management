contract_types = {
    1: 'sell',
    2: 'mortgage',
    3: 'rent',
    4: 'mortgage_and_rent',
}

roles = {
    1: 'seller',
    2: 'consultant',
}

user_objects = {}
contract_objects = {}
property_objects = {}