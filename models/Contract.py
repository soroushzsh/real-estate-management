from .storage import contract_objects


class Contract:
    id = 0

    def __init__(self, price, ctype):
        self.price = price
        self.ctype = ctype

    @classmethod
    def contract_options(cls):
        print("Choose contract type: ")
        print("1- Sell contract")
        print("2- Mortgage contract")
        print("3- Rent contract")
        print("4- Mortgage and rent contract")

        contract_type = int(input(">"))
        if contract_type == 1:
            new_contract = Sell.make_sell_contract()
            new_contract.save()
            return new_contract
        elif contract_type == 2:
            new_contract = Mortgage.make_mortgage_contract()
            new_contract.save()
            return new_contract
        elif contract_type == 3:
            new_contract = Rent.make_rent_contract()
            new_contract.save()
            return new_contract
        elif contract_type == 4:
            new_contract = MortgageAndRent.make_mortgage_and_rent_contract()
            new_contract.save()
            return new_contract
        else:
            print("Invalid option. try again")
            return Contract.contract_options()

    def save(self):
        contract_objects[self.id] = self

    @staticmethod
    def show_all_contracts(self):
        pass


class Sell(Contract):
    id = 0

    def __init__(self, price):
        super().__init__(price, ctype=1)
        self.id = Sell.id

        Sell.id += 1

    @classmethod
    def make_sell_contract(cls):
        price = int(input("Enter sell price: "))
        return cls(price)

    def __str__(self):
        return f"This is sell contract no: {self.id}"


class Mortgage(Contract):
    id = 0

    def __init__(self, price):
        super().__init__(price, ctype=2)
        self.id = Mortgage.id

        Mortgage.id += 1

    @classmethod
    def make_mortgage_contract(cls):
        price = int(input("Enter mortgage price: "))
        return cls(price)

    def __str__(self):
        return f"This is mortgage contract no: {self.id}"


class Rent(Contract):
    id = 0

    def __init__(self, price):
        super().__init__(price, ctype=3)
        self.id = Rent.id

        Rent.id += 1

    @classmethod
    def make_rent_contract(cls):
        price = int(input("Enter rent per month: "))
        return cls(price)

    def __str__(self):
        return f"This is rent contract no: {self.id}"


class MortgageAndRent(Contract):
    id = 0

    def __init__(self, mortgage, rent):
        super().__init__(mortgage, ctype=4)
        self.rent = rent
        self.id = MortgageAndRent.id

        MortgageAndRent.id += 1

    @classmethod
    def make_mortgage_and_rent_contract(cls):
        mortgage = int(input("Mortgage price: "))
        rent = int(input("Rent price: "))

        return cls(mortgage, rent)

    def __str__(self):
        return f"This is mortgage and rent contract no: {self.id}"