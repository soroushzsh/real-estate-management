from .storage import user_objects


class User:
    id = 0

    def __init__(self, first_name, last_name, email, phone, role):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.phone = phone
        self.role = role
        self.id = User.id

        User.id += 1

    @classmethod
    def options(cls):
        print("1- Select from old users")
        print("2- Add new user")
        # print("3- Delete user")
        option_selected = int(input(">"))

        if option_selected == 1:
            User.show_all_users()
            user_id = int(input("Select one: "))
            return user_id
        elif option_selected == 2:
            user = User.add_new_user()
            user.save()
            return user.id
        else:
            print("Invalid option selected.")
            return User.options()

    @classmethod
    def add_new_user(cls):
        first_name = input("First name: ")
        last_name = input("last name: ")
        email = input("Email: ")
        phone = input("Phone: ")

        print("Select User role: ")
        print("1- Seller\t2- Consultant")
        role = int(input(">"))

        return cls(first_name, last_name, email, phone, role)

    @classmethod
    def show_all_users(cls):
        for id, name in user_objects.items():
            print(f"{id}- {name}")

    def save(self):
        user_objects[self.id] = self

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
